#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QErrorMessage>
#include <QTime>
#include <QTimer>
#include <QKeyEvent>

#include "serialdisplay.h"
#include "preview.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    void closeEvent(QCloseEvent *event);
    ~MainWindow();
private:
    void enableWindow();
    void disableWindow();
    void keyPressEvent(QKeyEvent *event);
signals:
    void connectButtonClicked(const QString,const int);
    void sendLine(const int,const int, const QString);
    void sendLapLine(const int, const int, const int);
    void sendEmptyDisplay();
private slots:
    void contact();
private slots:
    void enableConnectButton();
    void disableConnectButton();
    void on_intensityComboBox_currentIndexChanged(int);
    void on_tabWidget_currentChanged(int);
    void sendTimeToWindow();
    void sendDecountToWindow();
    void on_sendHourButton_clicked();
    void on_sendLapButton_clicked();
    void on_sendLapDecountButton_clicked();
    void on_displayHoursCheckBox_stateChanged(int);
    void on_sendDecountButton_clicked();
    void on_sendTextButton_clicked();
    void refreshPortsComboBox(QList<QSerialPortInfo>&);
    void on_connectButton_clicked();
    void openWarningBox();
    void on_resetButton_clicked();
    void on_stopHourButton_clicked();
    void on_pauseDecountButton_clicked();
    void on_protocolComboBox_currentIndexChanged(int index);
    void on_line1Edit_returnPressed();
    void on_line2Edit_returnPressed();
    void on_line3Edit_returnPressed();
    void on_line4Edit_returnPressed();
    void on_previewCheckBox_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
    QMenu *m_helpMenu;
    QAction *m_contactAct;

    SerialDisplay *m_display;
    Preview *m_preview;
    QTimer *m_timer,*m_timerDecount;

    int m_intensity;
    bool m_sendTimeToDisplay = false;
    bool m_launchDecount = false;
};

#endif // MAINWINDOW_H
