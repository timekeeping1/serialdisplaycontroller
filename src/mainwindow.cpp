#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Serial Display Controller");
    m_contactAct = new QAction("Contact",this);
    m_contactAct->setStatusTip("contacts de l'application");
    connect(m_contactAct,&QAction::triggered,this,&MainWindow::contact);

    m_helpMenu = menuBar()->addMenu("Aide");
    m_helpMenu->addAction(m_contactAct);

    m_display = new SerialDisplay();

    m_timer = new QTimer;
    m_timer->start(100);
    m_timerDecount = new QTimer;

    connect(this->m_timer,SIGNAL(timeout()),this,SLOT(sendTimeToWindow()));
    connect(this->m_display,SIGNAL(warningNoComPort()),this,SLOT(openWarningBox()));
    connect(this,SIGNAL(connectButtonClicked(QString,int)),this->m_display,SLOT(connect(QString,int)));
    connect(ui->disconnectButton,SIGNAL(clicked()),this->m_display,SLOT(disconnect()));
    ui->refreshComButton->setIcon(style()->standardIcon(QStyle::SP_BrowserReload));
    connect(ui->refreshComButton,SIGNAL(clicked()),this->m_display,SLOT(refresh()));
    connect(this->m_display,SIGNAL(availablePortsRefreshed(QList<QSerialPortInfo>&)),
            this,SLOT(refreshPortsComboBox(QList<QSerialPortInfo>&)));
    connect(ui->disconnectButton,SIGNAL(clicked()),this,SLOT(enableConnectButton()));
    connect(this->m_display,SIGNAL(connected()),this,SLOT(disableConnectButton()));
    connect(this->m_display,SIGNAL(disconnected()),this,SLOT(enableConnectButton()));
    connect(this,SIGNAL(sendLine(int,int,QString)),this->m_display,SLOT(sendLineSlot(int,int,QString)));
    connect(this,SIGNAL(sendEmptyDisplay()),this->m_display,SLOT(sendEmptyDisplay()));
    connect(this,SIGNAL(sendLapLine(int,int,int)),this->m_display,SLOT(sendLapLineSlot(int,int,int)));

    disableWindow();
    ui->disconnectButton->setEnabled(false);

    ui->localTimeEdit->setReadOnly(true);

    m_intensity = ui->intensityComboBox->currentIndex()+1;
    for (int i=0;i<this->m_display->m_availablePorts.size();i++)
        ui->availableComComboBox->addItem(this->m_display->m_availablePorts[i].portName());
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_display;
    delete m_timer;
    delete m_timerDecount;
    delete m_helpMenu;
    delete m_contactAct;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    this->m_preview->close();
    QMainWindow::closeEvent(event);
}

void MainWindow::enableWindow()
{
    ui->resetButton->setEnabled(true);
    ui->localTimeEdit->setEnabled(true);
    ui->sendHourButton->setEnabled(true);
    ui->stopHourButton->setEnabled(false);
    ui->line1Edit->setEnabled(true);
    ui->line2Edit->setEnabled(true);
    ui->line3Edit->setEnabled(true);
    ui->line4Edit->setEnabled(true);
    ui->sendTextButton->setEnabled(true);
    if(ui->protocolComboBox->currentIndex()!=3 )ui->displayHoursCheckBox->setEnabled(true);
    ui->timeDecount->setEnabled(true);
    ui->sendDecountButton->setEnabled(true);
    ui->pauseDecountButton->setEnabled(false);
    ui->lapCount->setEnabled(true);
    ui->sendLapButton->setEnabled(true);
    ui->sendLapDecountButton->setEnabled(true);
}

void MainWindow::disableWindow()
{
    ui->resetButton->setEnabled(false);
    ui->localTimeEdit->setEnabled(false);
    ui->sendHourButton->setEnabled(false);
    ui->stopHourButton->setEnabled(false);
    ui->line1Edit->setEnabled(false);
    ui->line2Edit->setEnabled(false);
    ui->line3Edit->setEnabled(false);
    ui->line4Edit->setEnabled(false);
    ui->sendTextButton->setEnabled(false);
    ui->displayHoursCheckBox->setEnabled(false);
    ui->timeDecount->setEnabled(false);
    ui->sendDecountButton->setEnabled(false);
    ui->pauseDecountButton->setEnabled(false);
    ui->lapCount->setEnabled(false);
    ui->sendLapButton->setEnabled(false);
    ui->sendLapDecountButton->setEnabled(false);
}

void MainWindow::enableConnectButton()
{
    ui->connectButton->setEnabled(true);
    ui->disconnectButton->setDisabled(true);
    disableWindow();
    ui->protocolComboBox->setEnabled(true);
    ui->availableComComboBox->setEnabled(true);
}

void MainWindow::disableConnectButton()
{
    ui->disconnectButton->setEnabled(true);
    ui->connectButton->setDisabled(true);
    enableWindow();
    ui->protocolComboBox->setEnabled(false);
    ui->availableComComboBox->setEnabled(false);
}

void MainWindow::on_intensityComboBox_currentIndexChanged(int index)
{
    m_intensity = index+1;
}

void MainWindow::on_resetButton_clicked()
{
    m_sendTimeToDisplay = false;
    m_launchDecount = false;
    enableWindow();

    ui->line1Edit->setText("");
    ui->line2Edit->setText("");
    ui->line3Edit->setText("");
    ui->line4Edit->setText("");
    ui->displayHoursCheckBox->setChecked(false);
    ui->timeDecount->setTime(QTime(0,20,0,0));
    ui->lapCount->setValue(0);

    emit sendEmptyDisplay();
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    m_sendTimeToDisplay = false;
    m_launchDecount = false;
    if(ui->disconnectButton->isEnabled()) enableWindow();
    if(index==0)
    {
        this->m_timer->disconnect();
        connect(this->m_timer,SIGNAL(timeout()),this,SLOT(sendTimeToWindow()));
        ui->sendHourButton->setFocus();
    }
    else this->m_timer->disconnect(SIGNAL(timeout()));
    if(index==1)
    {
        ui->line1Edit->setFocus();
        ui->line1Edit->selectAll();
    }
    if(index==2)
    {
        this->m_timerDecount->disconnect();
        connect(this->m_timerDecount,SIGNAL(timeout()),this,SLOT(sendDecountToWindow()));
        m_timerDecount->start(10);
        ui->timeDecount->setFocus();
    }
    else this->m_timerDecount->disconnect(SIGNAL(timeout()));
    if(index==3)
    {
        ui->lapCount->setFocus();
        ui->lapCount->selectAll();
    }
}

void MainWindow::sendTimeToWindow()
{
    m_timer->start(1000);
    ui->localTimeEdit->setTime(QTime::currentTime());
    if(m_sendTimeToDisplay)
    {
        if(ui->protocolComboBox->currentIndex()!=3)
            emit sendLine(0,m_intensity,QTime::currentTime().toString("hh:mm:ss"));
        else
            emit sendLine(0,m_intensity,QTime::currentTime().toString("hh:mm "));
    }
}

void MainWindow::sendDecountToWindow()
{
    m_timerDecount->start(1000);
    if(m_launchDecount)
    {
        ui->timeDecount->setTime(ui->timeDecount->time().addSecs(-1));
        if(ui->displayHoursCheckBox->checkState()==2)
            emit sendLine(0, m_intensity, ui->timeDecount->time().toString("hh:mm:ss"));
        else
            emit sendLine(0, m_intensity, ui->timeDecount->time().toString(" mm:ss  "));
    }
}

void MainWindow::on_sendHourButton_clicked()
{
    m_sendTimeToDisplay = true;
    ui->sendHourButton->setEnabled(false);
    ui->stopHourButton->setEnabled(true);
    ui->stopHourButton->setFocus();

    ui->tabWidget->setTabEnabled(1,false);
    ui->tabWidget->setTabEnabled(2,false);
    ui->tabWidget->setTabEnabled(3,false);
}

void MainWindow::on_sendLapButton_clicked()
{
    emit sendLapLine(0, m_intensity, ui->lapCount->value());
    ui->lapCount->setFocus();
    ui->lapCount->selectAll();
}

void MainWindow::on_sendLapDecountButton_clicked()
{
    int lap=ui->lapCount->value();
    emit sendLapLine(0, m_intensity, lap);
    ui->lapCount->setValue(lap-1);
}

void MainWindow::on_displayHoursCheckBox_stateChanged(int arg1)
{
    if(arg1==2) ui->timeDecount->setDisplayFormat("hh:mm:ss");
    else ui->timeDecount->setDisplayFormat("mm:ss");
}

void MainWindow::on_sendDecountButton_clicked()
{
    m_launchDecount = true;
    ui->timeDecount->setDisabled(true);
    ui->sendDecountButton->setEnabled(false);
    m_timerDecount->start(10);
    ui->pauseDecountButton->setEnabled(true);
    ui->pauseDecountButton->setFocus();

    ui->tabWidget->setTabEnabled(0,false);
    ui->tabWidget->setTabEnabled(1,false);
    ui->tabWidget->setTabEnabled(3,false);
}

void MainWindow::on_sendTextButton_clicked()
{
    emit sendLine(0, m_intensity, ui->line1Edit->text());
    emit sendLine(1, m_intensity, ui->line2Edit->text());
    emit sendLine(2, m_intensity, ui->line3Edit->text());
    emit sendLine(3, m_intensity, ui->line4Edit->text());
}

void MainWindow::refreshPortsComboBox(QList<QSerialPortInfo> &availablePorts)
{
    ui->availableComComboBox->clear();
    for (int i=0;i<availablePorts.size();i++) ui->availableComComboBox->addItem(availablePorts[i].portName());
}

void MainWindow::on_connectButton_clicked()
{
    emit connectButtonClicked(ui->availableComComboBox->currentText(),ui->protocolComboBox->currentIndex());
}

void MainWindow::openWarningBox()
{
    QMessageBox::warning(this, "Attention", "Pas de port Série selectionné");
}

void MainWindow::on_stopHourButton_clicked()
{
    m_sendTimeToDisplay=false;
    ui->stopHourButton->setEnabled(false);
    ui->sendHourButton->setEnabled(true);
    ui->sendHourButton->setFocus();

    ui->tabWidget->setTabEnabled(1,true);
    ui->tabWidget->setTabEnabled(2,true);
    ui->tabWidget->setTabEnabled(3,true);
    emit sendEmptyDisplay();
}

void MainWindow::on_pauseDecountButton_clicked()
{
    m_launchDecount = false;
    ui->pauseDecountButton->setEnabled(false);
    ui->sendDecountButton->setEnabled(true);
    ui->timeDecount->setDisabled(false);
    ui->sendDecountButton->setFocus();

    ui->tabWidget->setTabEnabled(0,true);
    ui->tabWidget->setTabEnabled(1,true);
    ui->tabWidget->setTabEnabled(3,true);
}

void MainWindow::on_protocolComboBox_currentIndexChanged(int index)
{
    if(index==3)
    {
        ui->displayHoursCheckBox->setChecked(false);
        ui->timeDecount->setDisplayFormat("mm:ss");
        ui->displayHoursCheckBox->setEnabled(false);
    }
    else ui->displayHoursCheckBox->setEnabled(true);
}

void MainWindow::on_line1Edit_returnPressed()
{
    emit sendLine(0, m_intensity, ui->line1Edit->text());
}

void MainWindow::on_line2Edit_returnPressed()
{
    emit sendLine(1, m_intensity, ui->line2Edit->text());
}

void MainWindow::on_line3Edit_returnPressed()
{
    emit sendLine(2, m_intensity, ui->line3Edit->text());
}

void MainWindow::on_line4Edit_returnPressed()
{
    emit sendLine(3, m_intensity, ui->line4Edit->text());
}

void MainWindow::contact()
{
    QMessageBox::about(this, "Contacts",
                       "Pour toute information, contactez moi à l'adresse : florent.forget1@gmail.com");
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if((event->key() == Qt::Key_Enter)and(ui->disconnectButton->isEnabled()))
    {
        switch(ui->tabWidget->currentIndex())
        {
        case 0:
            if(ui->sendHourButton->isEnabled())
            {
                m_sendTimeToDisplay = true;
                ui->sendHourButton->setEnabled(false);
                ui->stopHourButton->setEnabled(true);
                ui->stopHourButton->setFocus();

                ui->tabWidget->setTabEnabled(1,false);
                ui->tabWidget->setTabEnabled(2,false);
                ui->tabWidget->setTabEnabled(3,false);
            }
            break;
        case 1:
            emit sendLine(0, m_intensity, ui->line1Edit->text());
            emit sendLine(1, m_intensity, ui->line2Edit->text());
            emit sendLine(2, m_intensity, ui->line3Edit->text());
            emit sendLine(3, m_intensity, ui->line4Edit->text());
            break;
        case 2:
            if(ui->sendDecountButton->isEnabled())
            {
                m_launchDecount = true;
                ui->timeDecount->setDisabled(true);
                ui->sendDecountButton->setEnabled(false);
                m_timerDecount->start(10);
                ui->pauseDecountButton->setEnabled(true);
                ui->pauseDecountButton->setFocus();

                ui->tabWidget->setTabEnabled(0,false);
                ui->tabWidget->setTabEnabled(1,false);
                ui->tabWidget->setTabEnabled(3,false);
            }
            break;
        case 3:
            emit sendLapLine(0, m_intensity, ui->lapCount->value());
            ui->lapCount->setFocus();
            ui->lapCount->selectAll();
            break;
        }

    }
}

void MainWindow::on_previewCheckBox_stateChanged(int arg1)
{
    if(arg1)
    {
        this->m_preview = new Preview();
        connect(this,SIGNAL(sendLine(int,int,QString)),this->m_preview,SLOT(sendLineSlot(int,int,QString)));
        connect(this,SIGNAL(sendEmptyDisplay()),this->m_preview,SLOT(sendEmptyDisplay()));
        connect(this,SIGNAL(sendLapLine(int,int,int)),this->m_preview,SLOT(sendLapLineSlot(int,int,int)));
    }
    else
    {
        this->m_preview->close();
        disconnect(this,SIGNAL(sendLine(int,int,QString)),this->m_preview,SLOT(sendLineSlot(int,int,QString)));
        disconnect(this,SIGNAL(sendEmptyDisplay()),this->m_preview,SLOT(sendEmptyDisplay()));
        disconnect(this,SIGNAL(sendLapLine(int,int,int)),this->m_preview,SLOT(sendLapLineSlot(int,int,int)));
    }
}
