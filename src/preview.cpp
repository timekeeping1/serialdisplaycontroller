#include "preview.h"
#include "ui_preview.h"

Preview::Preview(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Preview)
{
    ui->setupUi(this);

    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_DeleteOnClose);

    ui->previewL1->setAutoFillBackground(true);
    ui->previewL1->setAlignment(Qt::AlignCenter);
    QPalette palette = ui->previewL1->palette();
    palette.setColor(QPalette::WindowText,Qt::white);
    palette.setColor(QPalette::Window,Qt::black);
    ui->previewL1->setPalette(palette);

    ui->previewL2->setAutoFillBackground(true);
    ui->previewL2->setAlignment(Qt::AlignCenter);
    palette = ui->previewL2->palette();
    palette.setColor(QPalette::WindowText,Qt::white);
    palette.setColor(QPalette::Window,Qt::black);
    ui->previewL2->setPalette(palette);

    ui->previewL3->setAutoFillBackground(true);
    ui->previewL3->setAlignment(Qt::AlignCenter);
    palette = ui->previewL3->palette();
    palette.setColor(QPalette::WindowText,Qt::white);
    palette.setColor(QPalette::Window,Qt::black);
    ui->previewL3->setPalette(palette);

    ui->previewL4->setAutoFillBackground(true);
    ui->previewL4->setAlignment(Qt::AlignCenter);
    palette = ui->previewL4->palette();
    palette.setColor(QPalette::WindowText,Qt::white);
    palette.setColor(QPalette::Window,Qt::black);
    ui->previewL4->setPalette(palette);

    ui->previewL1->setTextFormat(Qt::RichText);
    ui->previewL1->setAlignment(Qt::AlignCenter);


    ui->previewL2->setTextFormat(Qt::RichText);
    ui->previewL2->setAlignment(Qt::AlignCenter);

    ui->previewL3->setTextFormat(Qt::RichText);
    ui->previewL2->setAlignment(Qt::AlignCenter);

    ui->previewL4->setTextFormat(Qt::RichText);
    ui->previewL2->setAlignment(Qt::AlignCenter);

    if(qApp->screenAt(QPoint(qApp->allWindows()[0]->x()+600,qApp->allWindows()[0]->y())) !=nullptr)
        this->move(qApp->allWindows()[0]->x()+350,qApp->allWindows()[0]->y());
    else this->move(qApp->allWindows()[0]->x()-260,qApp->allWindows()[0]->y());

    this->show();
}

Preview::~Preview()
{
    delete ui;
}

void Preview::sendLineSlot(const int index, const int, const QString data)
{
    if(index==0)
    {
        ui->previewL1->clear();
        ui->previewL1->setText(data);
    }
    if(index==1)
    {
        ui->previewL2->clear();
        ui->previewL2->setText(data);
    }
    if(index==2)
    {
        ui->previewL3->clear();
        ui->previewL3->setText(data);
    }
    if(index==3)
    {
        ui->previewL4->clear();
        ui->previewL4->setText(data);
    }
}

void Preview::sendLapLineSlot(const int index, const int, const int lap)
{
    if(index==0)
    {
        ui->previewL1->clear();
        if(lap<1) ui->previewL1->setText("Finish");
        else if(lap<10) ui->previewL1->setText(" "+QString::number(lap)+" Tours");
        else ui->previewL1->setText(QString::number(lap)+" Tours");
    }
    if(index==1)
    {
        ui->previewL2->clear();
        if(lap<1) ui->previewL2->setText("Finish");
        else if(lap<10) ui->previewL2->setText(" "+QString::number(lap)+" Tours");
        else ui->previewL2->setText(QString::number(lap)+" Tours");
    }
    if(index==2)
    {
        ui->previewL3->clear();
        if(lap<1) ui->previewL3->setText("Finish");
        else if(lap<10) ui->previewL3->setText(" "+QString::number(lap)+" Tours");
        else ui->previewL3->setText(QString::number(lap)+" Tours");
    }
    if(index==3)
    {
        ui->previewL4->clear();
        if(lap<1) ui->previewL4->setText("Finish");
        else if(lap<10) ui->previewL4->setText(" "+QString::number(lap)+" Tours");
        else ui->previewL4->setText(QString::number(lap)+" Tours");
    }
}

void Preview::sendEmptyDisplay()
{
    ui->previewL1->clear();
    ui->previewL2->clear();
    ui->previewL3->clear();
    ui->previewL4->clear();
}
