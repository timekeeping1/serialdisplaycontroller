#ifndef PREVIEW_H
#define PREVIEW_H

#include <QWidget>
#include <QWindow>
#include <QPoint>
#include <QGuiApplication>

namespace Ui {
class Preview;
}

class Preview : public QWidget
{
    Q_OBJECT

public:
    explicit Preview(QWidget *parent = nullptr);
    ~Preview();

private slots:
    void sendLineSlot(const int, const int, const QString);
    void sendLapLineSlot(const int, const int, const int);
    void sendEmptyDisplay();

private:
    Ui::Preview *ui;
};

#endif // PREVIEW_H
