#include "serialdisplay.h"
#include <QDebug>

SerialDisplay::SerialDisplay()
{
    QString line = "         ";
    for (int i=0;i<16;i++) m_emptyDisplayLines.append(line);
    m_availablePorts = QSerialPortInfo::availablePorts();
}

void SerialDisplay::connect(QString portName, int protocol)
{
    if(portName.isEmpty())
    {
        emit warningNoComPort();
        return;
    }
    m_serial.setPortName(portName);
    if(m_serial.open(QIODevice::WriteOnly))
    {
        emit connected();
    }
    m_serial.setBreakEnabled(false);

    switch(protocol)
    {
    case 0:
    {
        sendLine = &SerialDisplay::sendLineHL97x_98x;
        sendLapLine = &SerialDisplay::sendLapLineHL97x_98x;
        sendDisplay = &SerialDisplay::sendDisplayHL97x_98x;
        m_indexList = &m_indexListHL97x_98x;
        break;
    }
    case 1:
    {
        sendLine = &SerialDisplay::sendLineHL97x_98x;
        sendLapLine = &SerialDisplay::sendLapLineHL97x_98x;
        sendDisplay = &SerialDisplay::sendDisplayHL97x_98x;
        m_indexList = &m_indexListHL97x_98x;
        break;
    }
    case 2:
    {
        sendLine = &SerialDisplay::sendLineHL990;
        sendLapLine = &SerialDisplay::sendLapLineHL990;
        sendDisplay = &SerialDisplay::sendDisplayHL990;
        m_indexList = &m_indexListHL990_960;
        break;
    }
    case 3:
    {
        sendLine = &SerialDisplay::sendLineHL960;
        sendLapLine = &SerialDisplay::sendLapLineHL960;
        sendDisplay = &SerialDisplay::sendDisplayHL960;
        m_indexList = &m_indexListHL990_960;
        break;
    }
    default:
    {
        sendLine = &SerialDisplay::sendLineHL97x_98x;
        sendDisplay = &SerialDisplay::sendDisplayHL960;
        m_indexList = &m_indexListHL97x_98x;
        break;
    }
    }
    (this->*sendDisplay)(1,m_emptyDisplayLines);
}

void SerialDisplay::disconnect()
{
    sendEmptyDisplay();
    m_serial.waitForBytesWritten(10000);
    if(m_serial.isOpen()) m_serial.close();
    emit disconnected();
}

void SerialDisplay::refresh()
{
    disconnect();
    m_availablePorts = QSerialPortInfo::availablePorts();
    emit availablePortsRefreshed(m_availablePorts);
}

void SerialDisplay::sendLineSlot(const int index,const int intensity,const QString data)
{
    (this->*sendLine)(index+1,intensity,data);
}

void SerialDisplay::sendLapLineSlot(const int index, const int intensity, const int lap)
{
    (this->*sendLapLine)(index+1,intensity,lap);
}

void SerialDisplay::sendEmptyDisplay()
{
    for (int i=0;i<16;i++)
    {
        (this->*sendLine)(i+1,0,m_emptyDisplayLines[i]);
    }
}

void SerialDisplay::sendLineHL97x_98x(const int& index,const int& intensity, const QString& data)
{
    QString dataC = data;
    dataC.resize(8,' ');
    QByteArray dataToSend;
    dataToSend.append(0x02);
    dataToSend.append((*m_indexList)[index]);
    dataToSend.append(QString::number(intensity));
    dataToSend.append(dataC);
    dataToSend.append("\n");
    m_serial.write(dataToSend);
}

void SerialDisplay::sendLineHL990(const int& index,const int& intensity, const QString& data)
{
    QString dataC = data;
    dataC.resize(8,' ');
    QByteArray dataToSend;
    dataToSend.append(0x02);
    dataToSend.append("L");
    dataToSend.append((*m_indexList)[index]);
    dataToSend.append("\t");
    dataToSend.append("A");
    dataToSend.append(dataC);
    dataToSend.append("\n");
    m_serial.write(dataToSend);
}

void SerialDisplay::sendLineHL960(const int& index,const int& intensity, const QString& data)
{
    QString dataC = data;
    dataC.resize(6,' ');
    QByteArray dataToSend;
    dataToSend.append(0x02);
    dataToSend.append("L");
    dataToSend.append((*m_indexList)[index]);
    dataToSend.append("\t");
    dataToSend.append("A");
    dataToSend.append("   ");
    dataToSend.append(dataC);
    dataToSend.append("\t");
    dataToSend.append("\n");
    m_serial.write(dataToSend);
}

void SerialDisplay::sendLapLineHL97x_98x(const int& index,const int& intensity, const int& lap)
{
    QString dataC;
    if(lap==0) dataC = " Finish ";
    else if(lap<10) dataC = " " + QString::number(lap) + " Tours";
    else dataC = QString::number(lap) + " Tours";
    QByteArray dataToSend;
    dataToSend.append(0x02);
    dataToSend.append((*m_indexList)[index]);
    dataToSend.append(QString::number(intensity));
    dataToSend.append(dataC);
    dataToSend.append("\n");
    m_serial.write(dataToSend);
}

void SerialDisplay::sendLapLineHL990(const int& index,const int& intensity, const int& lap)
{
    QString dataC;
    if(lap==0) dataC = " Finish  ";
    else if(lap<10) dataC = " " + QString::number(lap) + " Tours ";
    else dataC = QString::number(lap) + " Tours ";
    QByteArray dataToSend;
    dataToSend.append(0x02);
    dataToSend.append("L");
    dataToSend.append((*m_indexList)[index]);
    dataToSend.append("\t");
    dataToSend.append("A");
    dataToSend.append(dataC);
    dataToSend.append("\n");
    m_serial.write(dataToSend);
}

void SerialDisplay::sendLapLineHL960(const int& index,const int& intensity, const int& lap)
{
    QString dataC;
    if(lap==0) dataC = "Finish";
    else if(lap<10) dataC = " " + QString::number(lap) + " Tr.";
    else dataC = QString::number(lap) + " Tr.";
    QByteArray dataToSend;
    dataToSend.append(0x02);
    dataToSend.append("L");
    dataToSend.append((*m_indexList)[index]);
    dataToSend.append("\t");
    dataToSend.append("A");
    dataToSend.append("   ");
    dataToSend.append(dataC);
    dataToSend.append("\t");
    dataToSend.append("\n");
    m_serial.write(dataToSend);
}

void SerialDisplay::sendDisplayHL97x_98x(const int& intensity, const QStringList& linesToDisplay)
{
    for (int i=0;i<16;i++)
    {
        (this->*sendLine)(i+1,intensity,linesToDisplay[i]);
    }
}

void SerialDisplay::sendDisplayHL990(const int& intensity, const QStringList& linesToDisplay)
{
    for (int i=0;i<16;i++)
    {
        (this->*sendLine)(i+1,intensity,linesToDisplay[i]);
    }
}

void SerialDisplay::sendDisplayHL960(const int& intensity, const QStringList& linesToDisplay)
{
    for (int i=0;i<16;i++)
    {
        (this->*sendLine)(i+1,intensity,linesToDisplay[i]);
    }
}

