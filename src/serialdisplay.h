#ifndef SERIALDISPLAY_H
#define SERIALDISPLAY_H

#include <QObject>
#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QMessageBox>


class SerialDisplay : public QObject
{
    Q_OBJECT
public:
    SerialDisplay();
    void (SerialDisplay::*sendDisplay)(const int&, const QStringList&);
    void (SerialDisplay::*sendLine)(const int&, const int&, const QString&);
    void (SerialDisplay::*sendLapLine)(const int&, const int&, const int&);

private slots:
    void disconnect();
    void connect(QString,int);
    void refresh();
    void sendLineSlot(const int, const int, const QString);
    void sendLapLineSlot(const int, const int, const int);
    void sendEmptyDisplay();

signals:
    void connected();
    void disconnected();
    void availablePortsRefreshed(QList<QSerialPortInfo>&);
    void warningNoComPort();

private:
    QSerialPort m_serial;
    QSerialPortInfo m_serialInfo;
    void sendLineHL97x_98x(const int&, const int&, const QString&);
    void sendLineHL990(const int&, const int&, const QString&);
    void sendLineHL960(const int&, const int&, const QString&);
    void sendLapLineHL97x_98x(const int&, const int&, const int&);
    void sendLapLineHL990(const int&, const int&, const int&);
    void sendLapLineHL960(const int&, const int&, const int&);
    void sendDisplayHL97x_98x(const int&, const QStringList&);
    void sendDisplayHL990(const int&, const QStringList&);
    void sendDisplayHL960(const int&, const QStringList&);

public:
    QList<QSerialPortInfo> m_availablePorts;
    QStringList m_emptyDisplayLines;

private:
    QStringList *m_indexList;
    QStringList m_indexListHL97x_98x = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G"};
    QStringList m_indexListHL990_960 = {"0","1","2","3","4","5","6","7","8","9",":",";","<","=",">","?","@"};
};

#endif // SERIALDISPLAY_H
